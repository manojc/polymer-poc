Dependencies - node.

1.  Install polyserve npm package globally using 'npm install polyserve -g' command.
2.  Install bower packages using 'bower install' command.
3.  Go to root directory and run the application using 'polyserve' command.
4.  Home page - http://localhost:8080/components/polymer-poc/home.html